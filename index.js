var i18n = require('i18n-yaml');

module.exports = function(ops){

  function isObject(obj) {
    return (typeof obj === "object") && (obj !== null);
  }

  function __() {
    var args = arguments;
    if (typeof args[0] === 'string' && this.l) {
      args[0] = {
        phrase: arguments[0],
        locale: this.l
      };
    }
    return i18n.__.apply(i18n, args);
  }

  return function (files, ms, done) {
    ops.directory = ms.path(ops.directory);

    // passing options to i18n
    i18n.configure(ops);

    for (var file in files) {
      files[file].__  = __.bind(files[file]);
    }

    done();
  };
};